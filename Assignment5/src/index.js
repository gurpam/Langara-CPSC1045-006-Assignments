//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function addition(a, b){
    return a + b;
}


// Question 2;

// Variables:

// let minN;
// let maxN;

// Expression:

// minN<=number && number<=maxN;
function range(minN, num, maxN) {
    return minN <= num && num <= maxN;
}